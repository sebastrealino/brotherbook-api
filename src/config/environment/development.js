// Develop specific configuration
// ===================================

export default {
  httpPort: 3000,
  // Not using
  httpsPort: 3500, 
  // MongoDB connection options
  mongo: {
    // Mongo Server
    uri: 'mongodb+srv://apiaccess:yCJkpAoY8Lh3wAQu@brotherbookcluster.dqsgo.mongodb.net/bb_dev?retryWrites=true&w=majority', 
  },
  // secretKey for JWT
  secretKey: 'BR00Th3RB00K_5Zu5Ak',
  paginateOptions: {
    limit: 15,
  },
};
