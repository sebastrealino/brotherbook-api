import mongoose from 'mongoose';
import mongoosePaginate from 'mongoose-paginate-v2';

const bookSchema = new mongoose.Schema({
  name: {
    type: String,
  },
  description: {
    type: String,
  },
  rating: {
    type: Number,
  },
  isAvaiable: {
    type: Boolean,
    default: true,
  },
  _owner: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
  },
  _lentTo: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
  },
}, { timestamps: true });

bookSchema.plugin(mongoosePaginate);

export default mongoose.model('Book', bookSchema);
