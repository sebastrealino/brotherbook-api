// import Book from '../models/book';
import httpStatus from 'http-status';
import User from '../models/user';
import Book from '../models/book';
import { handleEntityNotFound, handleError, respondWithResult } from '../utils/requestHelpers';
import { QueryType } from '../utils/constants';

export async function createBook(req, res) {
  const userId = req.auth._id;
  try {
    const user = await User.findById(userId);
    await handleEntityNotFound()(user);
    const newBook = new Book(req.body);
    newBook._owner = user._id;
    await newBook.save();
    user._myBooks = user._myBooks ? user._myBooks.push(newBook) : newBook;
    await user.save();

    return await respondWithResult(res, httpStatus.CREATED)(newBook);
  } catch (error) {
    return handleError(res)(error);
  }
}

export async function listBooks(req, res) {
  const params = req.swagger.params;
  const userId = req.auth._id;
  const listBy = params.listBy.value || '';
  const page = params.page.value || 1;
  try {
    let query;
    switch (listBy) {
      case QueryType.MY:
        query = { _owner: userId };
        break;
      case QueryType.LENT:
        query = { _owner: userId, isAvaiable: false };
        break;
      case QueryType.BORROWED:
        query = { _lentTo: userId };
        break;
      default:
        query = {};
        break;
    }
    const options = {
      page,
      populate: {
        path: '_owner _lentTo',
        select: '-password',
      },
      sort: { updatedAt: -1 },
    };
    const books = await Book.paginate(query, options);
    return await respondWithResult(res)(books);
  } catch (error) {
    return handleError(res)(error);
  }
}


export async function patchBook(req, res) {
  const bookId = req.swagger.params.id.value;
  const userId = req.auth._id;
  try {
    const book = await Book.findById(bookId);
    await handleEntityNotFound()(book);
    book._lentTo = book.isAvaiable ? userId : undefined;
    book.isAvaiable = !book.isAvaiable;
    book.save();
    return respondWithResult(res)(book);
  } catch (error) {
    return handleError(res)(error);
  }
}
